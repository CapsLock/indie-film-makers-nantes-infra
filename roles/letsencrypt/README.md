Letsencrypt
===========

Voir aussi: *nginx*

Installe certbot, génère et vérifie les certificats pour une liste de
domaines. Le choix est fait de ne générer qu'un certificat multi-domaine plutôt
que plusieurs certificats.

- Si **lighttpd** est utilisé, le configure pour répondre aux challenges ACME
- Si **nginx** est utilisé, cette tâche incombe (décombe ?) au rôle *nginx*.

**Ne fonctionne que sous Jessie**


Variables
---------

- *letsencrypt-cert-domains* : Liste des domaines sur lesquels on utiliser
  letsencrypt (minimum un).
- *letsencrypt-httpd* : Doit être mentionné (*nginx* ou *lighttpd*)

Dependances
-----------

- *debian-base* (pour les backports)


TODO
----

- harmonisation de la syntaxe yaml
