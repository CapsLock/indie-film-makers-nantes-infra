Nginx
=====

Voir aussi rôle *letsencrypt*

Installe et configure un serveur nginx et ses vhosts.

Variables
---------

### Obligatoire

- *nginx_vhosts* : une liste de vhosts, les propriétés applicable à chaque
  élément de la liste sont décrits dans *Configuration des vhosts*.


### Optionelle

- *nginx_letsencrypt_webroot* : où sera stocké le dossier *.well-known* contenant
  les preuves des challenges letsencrypt (par défaut */var/www*).
- *nginx_ssl_fullchain_path* : chemin la chaine de certificats SSL à utiliser
  (format PEM), pointe par défaut sur un snakeoil
- *nginx_ssl_privkey_path* : chemin vers la clef privée SSL à utiliser (format
  PEM), pointe par défaut sur un snakeoil

Configuration des vhosts
------------------------

Plusieurs clefs sont disponibles pour chaque vhost de la liste *nginx_vhosts*.

### Obligatoires

- *domain* : le nom de domaine concerné
- *type* : `http` ou `https`

### Optionelles

- *features* : une liste pouvant contenir une ou plusieurs fonctonalités sur le domaines :
  - *letsencrypt-check* : pour répondre aux challenges ACME de letsencrypt.
  - *ssl-redirection* : pour rediriger un domaine en http vers sa version https
  - *no-robots* : pour désactiver l'indexation du vhost en entier
- *locations* : une liste de « dossiers » contenus dans ce vhost (dans bien des
cas, vous n'en aurez qu'un : « / »)
  - *path*: le chemin du dossier (ex: `/`, `/images/`)
  - *type*: doit prendre une valeur parmis :
    - *fastcgi* : renvoie le trafic à un serveur fastcgi dont l'adresse est fournie
      dans *target*
    - *reverse-proxy* : proxifie le trafic vers un serveur HTTP dont l'adresse
      est précisée dans *target*
    - *reverse-proxy-websockets* : idem, mais proxifie égalemnt les websockets
    - *static* : sert un dossier de fichiers statiques dont le chemin est
      précisé dans *target*
    - *domain-redirection* : redirige (301) vers un autre domaine precisé dans
      *target*
  - *target* : selon le *type*.
  - *vars*: une liste d'éléments de couples clefs/valeurs étant ajoutés
    tel-quels à la configuration nginx du *path*.


Exemple
-------

```
nginx_vhosts:
    - domain: www.example.com
      type: http
      features:
        - letsencrypt-check
        - ssl-redirection

    - domain: www.example.com
      type: https
      locations:
        - path: /
          type: static
          target: "/var/www/website"

    - domain: "app.example.com"
      type: https
      locations:
        - path: /
          type: fastcgi
          target: 127.0.0.1:8000
          vars:
            - [client_max_body_size, 0]
            - [access_log, "/var/log/nginx/app.access.log"]
            - [error_log, "/var/log/nginx/app.error.log"]

```
