Dropbear FDE
============

Un usage de dropbear dédié au déchiffrement de disque à distance.
Largement inspiré de https://wiki.faimaison.net/doku.php?id=debian-fde-dropbear

Ce rôle touche également aux paramètres d'*initramfs*.

Variables
---------

### Utilisateurs autorisés

On réutilise les clefs SSH gérées pour les besoins du rôle *authorized_users*,

- *dropbear_fde_users*: liste les utilisateurs autorisés à déchiffrer la machine
à distance.

### Configuration réseau

Si votre configuration réseau est simple (une seule interface, une simple route
par défaut), les paramètres par défaut devraient être corrects.

Sinon, vous *voulez* changer ces variables :

- *dropbear_fde_initramfs_ip*
- *dropbear_fde_initramfs_netmask*
- *dropbear_fde_initramfs_gateway*
- *dropbear_fde_initramfs_interface*


Exemple
-------

    - hosts: servers
      vars:
        dropbear_fde_users:
          - raymunda
          - klauss
