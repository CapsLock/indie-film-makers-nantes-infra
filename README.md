# Utiliser le playbook Ansible Indie film makers

Commencer par installer Ansible (>=2.0), puis :

    cd adminsys/ansible
    ansible-playbook -K playbook.yml

Note : ceci exécute l'intégralité du playbook, avec tous les rôles pour toutes
les machines, ce qui n'est en général pas ce qu'on cherche à faire.

Pour n'exécuter que certains rôles correspondant à un groupe de
machines, il est recommandé d'utiliser l'option `-l`, par exemple :

    ansible-playbook -K -l cloudron playbook.yml

Pour n'exécuter que certaines tâches, on peut combiner l'option `-l`
avec l'option `-t`, par exemple :

    ansible-playbook -K -l cloudron -t minimalist playbook.yml

# Les groupes de machines

Voir le fichier `ansible_hosts` qui définit les groupes de machines, puis voir
le fichier `playbook.yml` qui définit quels rôles sont appliqués à quels
groupes.
